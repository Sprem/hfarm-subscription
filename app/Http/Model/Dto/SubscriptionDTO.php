<?php

namespace App\Http\Model\Dto;

class SubscriptionDTO
{

    public $createdAt;
    public $validFrom;
    public $expiresOn;
    public $status;
    public $planType;
    public $currency;
    public $price;
    public $duration;

}
