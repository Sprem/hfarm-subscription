<?php

namespace App\Http\Model\Dao;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Http\Model\Dto\SubscriptionDTO;

class SubscriptionDAO extends Model
{

    /**
     * Handy method to return the active subscription for the given company id.
     * No exceptions are handled here. Please consider this as an simple example.
     * In a real world scenario there would be a proper try / catch that would
     * forward to SENTRY all the exception that happend in the model layer
     */
    public function getSubscription($idCompany)
    {

        $subscriptions = DB::select('select s.created_at, s.valid_from, s.expires_on, s.status,
                                        p.plan_type, p.currency, p.price, p.duration
                                        from Subscription s join Plan p on s.id_plan = p.id_plan
                                        where s.status = ? and s.id_company = ?', ["valid", $idCompany]);

        if(null == $subscriptions) return;

        $subscription = $subscriptions[0];

        $subscriptionDTO = new SubscriptionDTO();

        $subscriptionDTO->createdAt = $subscription->created_at;
        $subscriptionDTO->validFrom = $subscription->valid_from;
        $subscriptionDTO->expiresOn = $subscription->expires_on;
        $subscriptionDTO->status = $subscription->status;
        $subscriptionDTO->planType = $subscription->plan_type;
        $subscriptionDTO->currency = $subscription->currency;
        $subscriptionDTO->price = $subscription->price;
        $subscriptionDTO->duration = $subscription->duration;

        return $subscriptionDTO;

    }


}
