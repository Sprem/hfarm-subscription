<?php

namespace App\Http\Middleware;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;

use Closure;

class BaseApiThrottle extends \Illuminate\Routing\Middleware\ThrottleRequests
{

    private $exceptionFormatter;

    /**
     * Create a 'too many attempts' response.
     *
     * @param  string $key
     * @param  int $maxAttempts
     * @return \Illuminate\Http\Response
     */
    protected function buildResponse($key, $maxAttempts)
    {

        $message = [
            "code" => 5700,
            "message" => "Too many attempts, please slow down the request",
        ];

        $retryAfter = $this->limiter->availableIn($key);

        $headers = $this->addCustomHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );

        return response()->json($message, 429)->withHeaders($headers);

    }

    /**
     * Add the limit header information to the given response.
     *
     * @param  int $maxAttempts
     * @param  int $remainingAttempts
     * @param  int|null $retryAfter
     * @return \Illuminate\Http\Response
     */
    protected function addCustomHeaders($maxAttempts, $remainingAttempts, $retryAfter = null)
    {
        $headers = [
            'X-RateLimit-Limit' => $maxAttempts,
            'X-RateLimit-Remaining' => $remainingAttempts,
        ];

        if (!is_null($retryAfter)) {
            $headers['Retry-After'] = $retryAfter;
            $headers['Content-Type'] = 'application/json';
        }

        return $headers;

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $maxAttempts = 60, $decayMinutes = 1, $prefix = "")
    {

        $key = $this->resolveRequestSignature($request);

        //Check the deafult throttle limit
        if ($this->limiter->tooManyAttempts($key, $maxAttempts, $decayMinutes)) {
            return $this->buildResponse($key, $maxAttempts);
        }

        $headers = $request->header();

        /**
         * Check for basic auth header server-to-server authentication.
         * If none / wrong credential are provided,
         * an error is returned and the request discarded.
         */
        if (array_key_exists('authorization', $headers)) {

            $basic = explode(" ", $headers['authorization'][0]);
            $basic = base64_decode($basic[1]);
            $basic = explode(":", $basic);
            $clientId = $basic[0];
            $clientSecret = $basic[1];

            if ($clientId === env("AUTH_CLIENT_ID") && $clientSecret === env("AUTH_SECRET_ID")) {
                $response = $next($request);
                return $this->addHeaders(
                    $response, $maxAttempts,
                    $this->calculateRemainingAttempts($key, $maxAttempts)
                );
            }
            response()->json($clientId, 429);

        }else{
            return $this->sendErrorMessage(5100);
        }

    }

    private function sendErrorMessage($code)
    {
        if(null == $this->exceptionFormatter)
            $this->exceptionFormatter = new ExceptionFormatter();
        return $this->exceptionFormatter->sendErrorResponse($code);
    }

}
