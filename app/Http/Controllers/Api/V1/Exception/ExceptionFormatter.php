<?php

namespace App\Http\Controllers\Api\V1\Exception;

/**
 * This trait provides helpufl methods for returning a formatted error message
 */
class ExceptionFormatter
{

    /**
     * @return a formatted error code with details about the exception
     */
    public function sendErrorResponse($customCode, $errors = [])
    {

        $json = [
            "code" => $customCode,
            "message" => self::getEnglishErrorMessage($customCode),
            "errors" => $errors,
        ];

        return response()->json(
            $json,
            $this->getHttpStatusCode($customCode)
        );
    }

    private function getHttpStatusCode($errorCode)
    {
        $codes = array(+
            5100 => 401, //unauthorized
            5200 => 400, //bad request
            5300 => 500 //internal error
        );
        return (isset($codes[$errorCode])) ? $codes[$errorCode] : '500';
    }

    private function getEnglishErrorMessage($errorCode)
    {
        $codes = array(
            5100 => 'Unauthorized',
            5200 => 'Bad request',
            5300 => 'Internal server error'
        );
        return (isset($codes[$errorCode])) ? $codes[$errorCode] : '';
    }
}
