<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Model\Dao\SubscriptionDAO;
use App\Http\Controllers\Api\V1\Exception\ExceptionFormatter;
use Validator;
use Log;

class Subscription extends BaseController
{

    private $exceptionFormatter;

    /**
     * Very simple and primitive implementation. Not interest in the architecture of this
     * very small piece of code. The concept here is: SEPARATION OF RESPONSIBILITY. We
     * want to separate the responsibility of managing subscriptions from the rest of the
     * API. Through this approach we obtain easier testing, deploy and we limit the
     * logical size of this micro service that can live as standalone component in any
     * machine we want it deployed, so that we can scale vertically without considering
     * the workload produced by the main APIs.
     */
    public function getSubscription(Request $request)
    {

        try {

            //Validate the request
            $parameters = [
                'id_company' => 'required|string|size:10'
            ];
            $validation = Validator::make($request->all(), $parameters);
            if ($validation->fails()) {
                return $this->sendErrorMessage(5200);
            }

            $subscriptionDAO = app()->make('\App\Http\Model\Dao\SubscriptionDAO');
            $subscription = $subscriptionDAO->getSubscription($request["id_company"]);

            return response()->json($subscription, 200);

        } catch (\Exception $e) {
            return $this->sendErrorMessage(5300);
        }

    }

    private function sendErrorMessage($code)
    {
        if(null == $this->exceptionFormatter)
            $this->exceptionFormatter = new ExceptionFormatter();
        return $this->exceptionFormatter->sendErrorResponse($code);
    }

}
